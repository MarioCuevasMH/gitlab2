/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tarea_gitlab2;
import java.util.Scanner;
/**
 *
 * @author DAM107
 */
public class GitLab2 {

    /**
     * @param args the command line arguments
     */
    
    //METODO MAIN
    public static void main(String[] args) {
        Scanner teclado=new Scanner(System.in);
        int eleccion, eleccion2;
        
        do{
            //MENÚ    
            System.out.println("¿Qué desea hacer?");
            System.out.println(" 1.-Cambio de horas a segundos\n 2.-Cambio de kilometros a metros\n 3.-Cambio de Km/h a m/s\n 4.-Cambio de Unidades Imperiales\n 5.-Salir");
            eleccion=teclado.nextInt();

            //METODOS 
            switch(eleccion){
                case 1:
                    System.out.println("¿Cuantas horas quieres pasar a segundos?");
                    int hora=teclado.nextInt();
                    System.out.println(hora+" horas son "+hora*3600+" segundos");
                    break;

                case 2:
                    System.out.println("¿Cuantos Km quieres pasar a metros?");
                    double km=teclado.nextDouble();
                    System.out.println(km+" Km son "+km*1000+" metros");
                    break;

                case 3:
                    System.out.println("Introduce Km/h para pasar a m/s:");
                    int kmh=teclado.nextInt();
                    System.out.println(kmh+" Km/h es equivalente a "+kmh/3.6+" m/s");
                    break;
                    
                case 4:
                    do{
                        System.out.println("¿Que cambio quieres realizar?");
                        System.out.println(" 1.-Cambio de pies a cm\n 2.-Cambio de pulgadas a mm\n 3.-Cambio de millas a Km\n 4.-Volver");
                        eleccion2=teclado.nextInt();
                        
                        switch(eleccion2){
                            case 1:
                                System.out.println("¿Cuantos pies quieres pasar a cm?");
                                int pies=teclado.nextInt();
                                System.out.println(pies+" pies son "+pies*30.48+" centímetros");
                                break;
                            
                            case 2:
                                System.out.println("¿Cuantas pulgadas quieres pasar a mm?");
                                int pul=teclado.nextInt();
                                System.out.println(pul+" pulgadas son "+pul/0.39+" milímetros");
                                break;
                                
                            case 3:
                                System.out.println("¿Cuantas millas quieres pasar a Km?");
                                int milla=teclado.nextInt();
                                System.out.println(milla+" millas son "+milla/0.6214+" Km");
                                break;
                            
                            case 4:
                                System.out.println("VOLVIENDO...");
                                break;
                        }
                    
                    }while(eleccion2!=4);
                    break;    
                    
                case 5:
                    System.out.println("SALIENDO...");
            }
        }while(eleccion!=5);
    }
    
}
